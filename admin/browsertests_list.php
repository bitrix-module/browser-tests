<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

use Bitrix\Main\Localization\Loc;
use Tests\BrowserTests\Check;

\Bitrix\Main\Loader::includeModule('browsertests');

IncludeModuleLangFile(__FILE__);

$sTableID = "browsertests_list";
$oSort = [];

$check = new Check();

$lAdmin = new CAdminUiList($sTableID, $oSort);

$arHeader[] = array(
    "id" => "NAME",
    "content" => Loc::getMessage("BROWSERTEST_TESTS_NAME"),
    "sort" => "name",
    "default" => true
);
$arHeader[] = array(
    "id" => "LAST_RUN",
    "content" => Loc::getMessage("BROWSERTEST_TESTS_LAST_RUN"),
    "sort" => "last_run",
    "default" => true
);
$arHeader[] = array(
    "id" => "RESULT",
    "content" => Loc::getMessage("BROWSERTEST_TESTS_RESULT"),
    "sort" => "result",
    "default" => true
);
$lAdmin->AddHeaders($arHeader);

//перебор тесткейсов
$arTestCaseList = glob($_SERVER['DOCUMENT_ROOT'] . "/local/modules/browsertests/lib/testcase/*.php");
foreach ($arTestCaseList as $TestCase) {
    $className = "Tests\\BrowserTestCase\\" . pathinfo($TestCase)['filename'];
    $objTestCase = new $className();
    $log = \Tests\BrowserTests\BrowserTestsHistoryTable::getList([
        'select' => ['DATE', 'RESULT'],
        'filter' => ['NAME' => pathinfo($TestCase)['filename']],
        'order' => ['DATE' => 'DESC']
    ])->fetch();
    $rows['NAME'] = $objTestCase->name;
    $rows['LAST_RUN'] = $log['DATE']::convertFormatToPhp($log['DATE']);
    $rows['RESULT'] = $log['RESULT'];
    $lAdmin->AddRow(pathinfo($TestCase)['filename'], $rows);
}
//перебор тесткейсов

$lAdmin->AddFooter(
    [
        [
            "counter" => true,
            "title" => Loc::getMessage("MAIN_ADMIN_LIST_CHECKED"),
            "value" => count($arTestCaseList)
        ],
    ]
);

$lAdmin->CheckListMode();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

foreach ($check->error as $error) {
    CAdminMessage::ShowMessage($error['note']);
}

$lAdmin->DisplayList();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
