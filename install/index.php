<?php
/**
 * User: Denis Filimonov
 */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;
use \Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);
if (class_exists('BrowserTests')) {
    return;
}

Class BrowserTests extends \CModule
{
    public $MODULE_ID = "browsertests";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;

    function __construct()
    {
        $this->MODULE_VERSION = "1.0";
        $this->MODULE_VERSION_DATE = "2019-02-13 01:53:00";
        $this->MODULE_NAME = Loc::getMessage("BROWSERTEST_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("BROWSERTEST_MODULE_DESCRIPTION");
    }

    function DoInstall()
    {
        global $DB, $APPLICATION, $step;

        registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);

        $this->InstallEvents();
        $this->installFiles();
        $this->installDB();

        //$APPLICATION->IncludeAdminFile(GetMessage("FORM_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mymodule/install/step1.php");
    }

    function DoUninstall()
    {
        global $DB, $APPLICATION, $step;

        Loader::includeModule($this->MODULE_ID);

        $this->UnInstallEvents();
        $this->uninstallFiles();
        $this->uninstallDB();

        unregisterModule($this->MODULE_ID);
        //$APPLICATION->IncludeAdminFile(GetMessage("FORM_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mymodule/install/unstep1.php");

    }

    function installFiles()
    {
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"] . "/local/modules/browsertests/install/admin",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin",
            true, true
        );

        $GLOBALS['CACHE_MANAGER']->CleanDir('menu');
        \CBitrixComponent::clearComponentCache('bitrix:menu');
        return true;
    }

    function uninstallFiles()
    {
        DeleteDirFiles(
            $_SERVER["DOCUMENT_ROOT"] . "/local/modules/browsertests/install/admin/",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin"
        );

        $GLOBALS['CACHE_MANAGER']->CleanDir('menu');
        \CBitrixComponent::clearComponentCache('bitrix:menu');
        return true;
    }

    function installDB()
    {
        if(!Application::getConnection(\Tests\BrowserTests\BrowserTestsHistoryTable::getConnectionName())->
        isTableExists(Base::getInstance('\Tests\BrowserTests\BrowserTestsHistoryTable')->getDBTableName())
        ) {
            Base::getInstance('\Tests\BrowserTests\BrowserTestsHistoryTable')->createDbTable();
        }
    }

    function uninstallDB()
    {
        Application::getConnection(\Tests\BrowserTests\BrowserTestsHistoryTable::getConnectionName())->
        queryExecute('drop table if exists ' . Base::getInstance('\Tests\BrowserTests\BrowserTestsHistoryTable')->
            getDBTableName());
    }

    function InstallEvents()
    {
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandlerCompatible('main', 'OnBuildGlobalMenu', $this->MODULE_ID, '\Tests\BrowserTests\Menu', 'onBuildGlobalMenu');
        return true;
    }

    function UnInstallEvents()
    {
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler('main', 'OnBuildGlobalMenu', $this->MODULE_ID, '\Tests\BrowserTests\Menu', 'onBuildGlobalMenu');
        return true;
    }

    public function setOptions()
    {
        Option::set('browsertests', 'ChromeDriverURL', 'http://localhost:9515');
    }

    public function dropOptions()
    {
        Option::delete('browsertests', ['ChromeDriverURL']);
    }
}