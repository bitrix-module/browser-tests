<?php
Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
    '\Tests\BrowserTests\Menu' => '/local/modules/browsertests/lib/menu.php',
    '\Tests\BrowserTests\Check' => '/local/modules/browsertests/lib/check.php',
    '\Tests\BrowserTests\TestCase' => '/local/modules/browsertests/lib/testcase.php',
    '\Tests\BrowserTests\BrowserTestsHistoryTable' => '/local/modules/browsertests/lib/browsertestshistory.php'
));
require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/vendor/autoload.php";