<?php
/**
 * User: Denis Filimonov
 * Date: 15.02.2019
 * Time: 21:22
 */
namespace Tests\BrowserTestCase;

use Tests\BrowserTests\TestCase;

class ExampleTest extends TestCase
{

    public function __construct()
    {
        $this->name = "Example test case";
        $this->description = "Example description test case";
    }

    /**
     * A basic browser test example.
     *
     * @throws \Throwable
     */
    public function testBasicExample()
    {
        $this->browse(function ($browser) {
            $browser->visit('/')
                ->assertPathIs('/home');
        });
    }
}