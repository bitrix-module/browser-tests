<?php

namespace Tests\BrowserTests;

use Bitrix\Main\Localization\Loc;
use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Bitrix\Main\Config\Option;

class TestCase extends BaseTestCase
{
    public $name;
    public $description;

    public static function prepare()
    {
        // static::startChromeDriver();
    }

    protected function driver()
    {
        return RemoteWebDriver::create(
            Option::get('browsertests', 'ChromeDriverURL'), DesiredCapabilities::chrome()
        );
    }
}
