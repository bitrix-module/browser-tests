<?php
/**
 * User: Denis Filimonov
 * Date: 16.02.2019
 * Time: 04:55
 */

namespace Tests\BrowserTests;

use Bitrix\Main\Entity;

class BrowserTestsHistoryTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'browser_tests_history';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
            ]),
            new Entity\StringField('NAME'),
            new Entity\DatetimeField('DATE'),
            new Entity\StringField('RESULT')
        );
    }
}
