<?php

namespace Tests\BrowserTests;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

class Check
{
    public $error = [];

    public function __construct()
    {
        if (Option::get('main', 'update_devsrv') != 'Y') {
            $this->error[] = ['note' => Loc::getMessage('BROWSERTTEST_ERROR_UPDATE_DEVSRV')];
        }
    }
}