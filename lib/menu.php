<?php
namespace Tests\BrowserTests;

use Bitrix\Main\Localization\Loc;

class Menu
{
    public static function onBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        global $APPLICATION;

        $moduleAccess = $APPLICATION->GetGroupRight('browsertests');

        if($moduleAccess < "R")
            return false;

        $menu = array(
            "parent_menu" => "global_menu_tests",
            'section' => 'BrowserTests',
            'sort' => 50,
            'text' => Loc::getMessage('BROWSERTEST_TESTS'),
            'items_id' => 'menu_browser_test',
            'url' => '',
            'more_url' => array(),
            'items' => array()
        );

        $menu['items'][] = array(
            'text' => Loc::getMessage('BROWSERTEST_TESTS_LIST'),
            'url' => 'browsertests_list.php?lang=' . LANGUAGE_ID . '&landing=Y'
        );

        $menu['items'][] = array(
            'text' => Loc::getMessage('BROWSERTEST_TESTS_PROP'),
            'url' => 'browsertests_prop.php?lang=' . LANGUAGE_ID . '&landing=Y'
        );

        return array(
            "global_menu_tests" => array(
                "menu_id" => "tests",
                "text" => Loc::getMessage("GM_TEST_TEXT"),
                "title" => Loc::getMessage("GM_TEST_TITLE"),
                "sort" => 10000,
                "items_id" => "global_menu_tests",
                "help_section" => "tests",
                "items" => array($menu)
            ));
    }
}